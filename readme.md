[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![pipeline status](https://gitlab.com/MarcTimperley/train/badges/master/pipeline.svg)](https://gitlab.com/MarcTimperley/train/commits/master)
[![coverage report](https://gitlab.com/MarcTimperley/train/badges/master/coverage.svg)](https://gitlab.com/MarcTimperley/train/commits/master)

# Train Times
Display upcoming train times and status for any direct route in the UK

#### Demo of the application can be found [here](https://train1.herokuapp.com/)

### Running the Application

To get up and running with the application locally, run the following:

 - `git clone https://gitlab.com/MarcTimperley/train`
 - `cd train`
 - create a config.json (see below)
 - `npm start`

### Running in Docker

To run the application in Docker, `docker run -p 5000:3000 -d marct/train` and open http://localhost:5000

###### config.json

Register for the API at [https://developer.transportapi.com/](https://developer.transportapi.com/signup)

Create the config.json file in the project root with the following contents:

`{
  "AppName": "Train Manager App ",
  "AppID": "[Your AppID]",
  "Key": "[Your Key]"
}`

This application will not work without this file in place

### Unit Tests

The unit tests will validate your API settings and that the REST endpoint is reachable. If JEST is not installed globally, run `npm i -g jest` to install it

To run the tests, simply type `npm test`

If they are not successful, check your config.json and firewall and proxy settings

To run the test coverage report, run `npm run-script test-coverage`

### Acknowledgements

Background image by <a href="https://pixabay.com/users/Free-Photos-242387/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=480466">Free-Photos</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=480466">Pixabay</a>

### ToDo

 - [ ] autocomplete
 - [ ] scheduled arrival
 - [X] status update
 - [ ] display improvements

### Versioning

We use [SemVer](http://semver.org/) for versioning.

### Authors

 * **Marc Timperley** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details

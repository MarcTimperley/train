/**
@file call rest API using request-promise-native
@module rest
**/
exports.callAPI = callAPI
const path = require('path')
const rp = require('request-promise-native')
const config = require(path.join(__dirname, '..', 'config.json'))

/**
 * call remote API generic function
 * @param  {string} url    URL for the API endpoint
 * @param  {string} key    Primary key of the results
 * @param  {string} fields Array of fields to return. If not specified, return all
 * @return {[type]}        [description]
 */
function callAPI(url, key, fields, callback) {
  console.log('calling ' + url)
  let results = []
  rp(`http://transportapi.com/v3/uk/${url}&app_id=${config.AppID}&app_key=${config.Key}`)
    .then(data => {
      let res = JSON.parse(data)[key]
      if (!Array.isArray(res)) res = res[Object.keys(res)]
      for (let data of res) {
        // console.log(data)
        let output = {}
        if (!fields) results.push(data)
        if (Array.isArray(fields)) {
          for (let field of fields) {
            // console.log(field)
            output[field] = data[field]
          }
        }
        // console.log(output)
        results.push(output)
      }
      if (callback) callback(results) // console.log(results)
    })
    .catch(err => {
      // console.log(err)
      if (callback) callback(err)
    })
}

'use strict'

setup()

/**
 * self-invoking function used to setup the page event handlers
 */
function setup() {
  $.getJSON('./stations.json', (data) => {
    let allStations = data
    let stations = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: allStations,
      sufficient: 1
    })
    stations.initialize()
    $('.typeahead').typeahead({
      minLength: 3,
      hint: false
    }, {
      name: 'name',
      display: 'name',
      source: stations.ttAdapter()
    }).bind('typeahead:select', function(ev, suggestion) {
      $('#' + ev.target.id + 'KEY').text(suggestion.code)
    })
  })

  $('#swap').on('click', function() {
    console.log('clicked swap')
    let t = $('#from').val()
    let tKEY = $('#fromKEY').text()
    $('#from').val($('#to').val())
    $('#to').val(t)
    $('#fromKEY').text($('#toKEY').text())
    $('#toKEY').text(tKEY)
    $('#results').html('')
  })
  $('#search').on('click', function() {
    console.log('clicked search for ' + $('#fromKEY').text() + ' to ' + $('#toKEY').text())
    if ($('#fromKEY').text() && $('#fromKEY').text() !== '' && $('#toKEY').text() && $('#toKEY').text() !== '' && $('#fromKEY').text() !== $('#toKEY').text()) {
      getTimes($('#fromKEY').text(), $('#toKEY').text())
    }
  })
}

/**
 * get stations matching the input
 * @param  {object} element JQuery element that triggered this input
 */
function getStation(element) {
  // console.log(element.id)
  if (element.value.length > 2) {
    // console.log(` calling /api/places?query=${element.value}`)
    $.getJSON(`/api/places?query=${element.value}`, (data) => {
      console.log(data)
      if (data.length === 1) {
        $(element).val(data[0].name)
        $('#' + element.id + 'KEY').text(data[0].station_code)
      }
    })
  }
}

/**
 * get schedule for the trains
 * @param  {string} source Three letter source station code (fromKEY)
 * @param  {string} dest Three letter destination station code (toKEY)
 */
function getTimes(source, dest) {
  // console.log(element.id)
  $('#loading').removeClass('invisible')
  $('#results').html('')
  $.getJSON(`/api/times?source=${source}&target=${dest}`, (data) => {
    displayTimes(data)
  })
}

/**
 * display the times on screen
 * @param  {Object} data train times from REST API
 */
function displayTimes(data) {
  let results = ''
  let counter = 0
  if (data.length === 0) results = '<tr><td>&nbsp</td></tr><tr><td>&nbsp</td><td colspan="3">No direct trains are available for the chosen stations</td></tr>'
  for (let trainTime of data) {
    let status = trainTime.status.split(' ').map(i => i[0].toUpperCase() + i.substring(1).toLowerCase()).join(' ')
    counter++
    results += `<tr><td>${counter}</td>`
    results += `<td>${trainTime.aimed_departure_time}</td>`
    results += `<td>${trainTime.expected_departure_time}</td>`
    results += `<td>${status}</td>`
    results += `<td>${trainTime.origin_name}</td>`
    results += `</tr>`
  }
  $('#loading').addClass('invisible')
  $('#results').html(results)
}

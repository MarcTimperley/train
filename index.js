'use strict'
/**
@file main web server handling routing and flow control
@module index
**/

const express = require('express')
const path = require('path')
const got = require('got')
const GA_TRACKING_ID = 'UA-149507083-1'
const trackEvent = (category, action, label, value) => {
  const data = {
    v: '1',
    tid: GA_TRACKING_ID,
    cid: '555',
    t: 'event',
    ec: category,
    ea: action,
    el: label,
    ev: value
  }
  console.log(data)
  console.log(`sending tracking info: ${data.ec}`)
  return got.post('http://www.google-analytics.com/collect', data)
}
const os = require('os')
const favicon = require('serve-favicon')
const rest = require(path.join(__dirname, 'lib', 'rest.js'))
var app = express()
app.enable('trust proxy')
var http = require('http').Server(app)
app.set('port', (process.env.PORT || 3000))

app.use(express.static(path.join(__dirname, 'public/')))
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.get('/', async (req, res) => {
  await trackEvent('Activity', 'homepage', '', '100')
  res.sendFile(path.join(__dirname, 'public/train.html'))
})
app.get('/api/places/', async (req, res) => {
  if (req.query && req.query.query) rest.callAPI(`places.json?query=${req.query.query}&type=train_station`, 'member', ['name', 'station_code'], (data) => res.send(data))
})
app.get('/api/times/', async (req, res) => {
  console.log(req.query)
  await trackEvent(
    'Activity',
    'Check times',
    `from ${req.query.source} to ${req.query.target}`,
    '100'
  )
  if (req.query && req.query.source && req.query.target) rest.callAPI(`train/station/${req.query.source}/live.json?calling_at=${req.query.target}&&from_offset=-PT01:00:00&to_offset=PT11:00:00`, 'departures', ['train_uid', 'aimed_departure_time', 'expected_departure_time', 'status', 'origin_name'], (data) => res.send(data))
})

http.listen(app.get('port'), function() {
  console.log(`server is running on ${os.hostname} in folder ${__dirname} listening on ${app.get('port')}`)
})

/*
  Simple set of example callsusing REST API
 function sampleCalls() {
  let source = process.argv[2] || 'BSK'
  let target = process.argv[3] || 'WAT'
  rest.callAPI(`places.json?query=SOU&type=train_station`, 'member', ['name', 'station_code'], display)
  rest.callAPI(`places.json?query=Brookwood&type=train_station`, 'member', null, display)
  rest.callAPI(`places.json?query=Waterloo&type=train_station`, 'member', ['name', 'station_code'], display)
  rest.callAPI(`train/station/${source}/live.json?calling_at=${target}`, 'departures', ['train_uid', 'aimed_departure_time', 'expected_departure_time', 'status', 'origin_name'], display)
  rest.callAPI(`train/station/${source}/live.json?calling_at=${target}`, 'departures', null, display)
  // rest(`http://transportapi.com/v3/uk/train/station/${source}/live.json?calling_at=${target}&station_detail=origin`, 'departures', ['train_uid', 'aimed_departure_time', 'expected_departure_time', 'status'])
}

function display(data) {
  console.log(data)
}

*/

const path = require('path')
const rest = require(path.join(__dirname, '../lib', 'rest.js'))

test('get REST API with parameter', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toEqual([{
      'station_code': 'BKO'
    }])
    done()
  }
  rest.callAPI(`/places.json?query=Brookwood&type=train_station`, 'member', ['station_code'], callback)
})

test('get REST API without parameter', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(data).toEqual([{
      type: 'train_station',
      name: 'Brookwood',
      latitude: 51.303756,
      longitude: -0.635748,
      accuracy: 100,
      station_code: 'BKO',
      tiploc_code: 'BRKWOOD'
    },
    {}
    ])
    done()
  }
  rest.callAPI(`/places.json?query=Brookwood&type=train_station`, 'member', null, callback)
})

test('get REST API with error', done => {
  function callback(data) {
    jest.setTimeout(30000)
    expect(JSON.stringify(data)).toMatch(/Server Error/)
    done()
  }
  rest.callAPI(`/places.json?query=XYZ&type=XYZ`, 'member', ['station_code'], callback)
})
